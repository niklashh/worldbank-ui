![meme](https://img.shields.io/badge/reaktor%20quality-/5-green.svg)

# `WorldBank-ui`

> [Try it out](http://niklassh.com)

A [Worldbank](https://www.worldbank.org) data viewer application
and proxy API server to communicate with Worldbank's indicator API
(SP.POP.TOTL and EN.ATM.CO2E.KT)

Also provides a redis cache

## Install dependencies

`yarn install`

## Start a development instance

`./dcup.sh`

## Start a production instance

- Edit `config.env` and set `FRONTEND_NODE_ENV=production`
- `./dcup.sh`

## Requirements

- `docker`
- `docker-compose`
- `yarn`

## API documentation (swagger)

Documentation for the API can be found from `{HOST}/api-docs/`
when frontend is set to development
