import React, {useContext} from 'react';
import PropTypes from 'prop-types';

import {CountryContext, OptionsContext} from '../Contexts';
import {Search} from '../Layouts';

const AutocompleteSearch = React.memo(props => {
  // console.debug('Search');
  const countries = useContext(CountryContext);
  const countryList = Object.keys(countries)
    .map(key => ({
      value: key,
      label: countries[key].name,
    }))
    .sort((a, b) =>
      a.label.localeCompare(b.label, 'en', {sensitivity: 'base'}),
    );
  const {
    selectedCountries,
    options: {selectCountries},
  } = useContext(OptionsContext);
  const selected =
    selectedCountries &&
    selectedCountries.map(({id, name}) => ({
      value: id,
      label: name,
    }));

  const handleChange = changed => {
    if (changed) {
      selectCountries(
        changed.map(({value}) => ({
          ...countries[value],
          id: value,
        })),
      );
    }
  };

  return React.useMemo(
    () => (
      <Search
        placeholder="Search..."
        inHeader={props.inHeader}
        handleChange={handleChange}
        options={countryList}
        value={selected}
      />
    ),
    [countries, selected],
  );
});

AutocompleteSearch.propTypes = {
  inHeader: PropTypes.bool,
};

export default AutocompleteSearch;
