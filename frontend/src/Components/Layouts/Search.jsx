import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {InputBase, Paper, MenuItem, Typography, Chip} from '@material-ui/core';
import {
  Search as SearchIcon,
  Clear,
  Cancel as CancelIcon,
} from '@material-ui/icons';
import {makeStyles, useTheme} from '@material-ui/styles';
import {fade} from '@material-ui/core/styles/colorManipulator';
import Select from 'react-select';
import {emphasize} from '@material-ui/core/styles/colorManipulator';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: '2px 4px',
    // XXX hard-coded breakpoint
    [theme.breakpoints.only('xs')]: {
      transition: theme.transitions.create('box-shadow'),
      '&:focus-within': {
        boxShadow: theme.shadows[6],
      },
    },
  },
  input: {
    '&&': {
      display: 'flex',
      paddingTop: 0.5 * theme.spacing.unit,
      paddingBottom: 0.5 * theme.spacing.unit,
      paddingRight: theme.spacing.unit,
      // XXX hard-coded breakpoint
      [theme.breakpoints.up('sm')]: {
        paddingLeft: theme.spacing.unit * 9,
        transition: [
          theme.transitions.create('width'),
          theme.transitions.create('min-width'),
        ],
        minWidth: 140,
        '&:focus-within': {
          minWidth: 280,
        },
      },
    },
  },
  inputBase: {
    '&&': {
      [theme.breakpoints.up('sm')]: {
        color: 'inherit',
      },
    },
  },
  searchHeader: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    // marginRight: 0,
    // marginLeft: theme.spacing.unit * 3,
    margin: theme.spacing.unit,
    width: 'auto',
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    width: '100%',
    '&>p': {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      left: 0,
      right: 0,
      // XXX hard-coded breakpoint
      [theme.breakpoints.up('sm')]: {
        left: theme.spacing.unit * 9,
      },
    },
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    position: 'absolute',
    paddingLeft: 2,
  },
  placeholder: {
    position: 'absolute',
    paddingLeft: 2,
    color: 'inherit',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  tooltip: {
    maxWidth: 300,
  },
  chip: {
    '&&': {
      margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
      [theme.breakpoints.up('sm')]: {
        backgroundColor: theme.palette.primary.dark,
        color: 'inherit',
      },
    },
    '&>span': {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      display: 'inline-block',
      [theme.breakpoints.up('sm')]: {
        maxWidth: 210,
      },
    },
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light'
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
      0.08,
    ),
  },
  clearDiv: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  clearIndicator: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit / 2,
  },
}));

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      noWrap
      {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function inputComponent({inputRef, ...props}) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <InputBase
      fullWidth
      className={props.selectProps.classes.inputBase}
      inputComponent={inputComponent}
      inputProps={{
        // TODO use `classes` attribute instead
        className: props.selectProps.classes.input,
        inputRef: props.innerRef,
        children: props.children,
        ...props.innerProps,
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option({children, ...props}) {
  // TODO fix lag
  const {onMouseMove, onMouseOver, ...rest} = props.innerProps;
  const newProps = Object.assign(props, {innerProps: rest});
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      {...newProps.innerProps}>
      <Typography noWrap color="textPrimary" variant="body1">
        {children}
      </Typography>
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="inherit"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return (
    <div className={props.selectProps.classes.valueContainer}>
      {props.children}
    </div>
  );
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

function Menu(props) {
  return (
    <Paper
      className={props.selectProps.classes.paper}
      elevation={8}
      {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

function ClearIndicator(props) {
  const {
    innerProps: {ref, ...restInnerProps},
  } = props;

  return (
    <div
      {...restInnerProps}
      className={props.selectProps.classes.clearDiv}
      ref={ref}>
      <Clear className={props.selectProps.classes.clearIndicator} />
    </div>
  );
}

// TODO fix convoluted passing of classes
// with useStyles
const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  ValueContainer,
  ClearIndicator,
  DropdownIndicator: null,
};
const Search = props => {
  const classes = useStyles();
  const theme = useTheme();

  const selectStyles = {
    input: base => ({
      ...base,
      color: props.inHeader ? 'inherit' : theme.palette.text.primary,
      '& input': {
        font: 'inherit',
      },
    }),
  };

  const SelectComponent = (
    <Select
      classes={classes}
      styles={selectStyles}
      options={props.options}
      components={components}
      value={props.value}
      onChange={(...args) => {
        props.handleChange(...args);
      }}
      placeholder={props.placeholder}
      isMulti
      isClearable
    />
  );

  if (props.inHeader) {
    return (
      <div className={classes.searchHeader}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        {SelectComponent}
      </div>
    );
  } else {
    return (
      <Paper className={classes.root} elevation={1}>
        {SelectComponent}
      </Paper>
    );
  }
};

Search.propTypes = {
  options: PropTypes.array.isRequired,
  value: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default Search;
