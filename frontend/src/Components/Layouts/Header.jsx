import React from 'react';
import {AppBar, Toolbar} from '@material-ui/core';

const Header = props => (
  <div style={{width: '100%'}}>
    <AppBar position="static">
      <Toolbar>{props.children}</Toolbar>
    </AppBar>
  </div>
);

export default Header;
