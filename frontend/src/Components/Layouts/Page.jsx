import React from 'react';
import {Paper} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    width: '100%',
    minHeight: '100%',
    overflow: 'hidden',
  },
}));

export default props => (
  <Paper square elevation={0} className={useStyles().paper}>
    {props.children}
  </Paper>
);
