import React, {useEffect, useState} from 'react';
import deepmerge from 'deepmerge';

import {fetchCountries} from '../../utils';

export const CountryContext = React.createContext();

export const CountryProvider = props => {
  // console.debug('CountryProvider');
  const appendCountries = data =>
    setCountries(prevCountries =>
      deepmerge(
        prevCountries,
        data.reduce((map, obj) => {
          map[obj.id] = {iso2Code: obj.iso2Code, name: obj.name};
          return map;
        }, {}),
      ),
    );

  const initialCountries = {};
  const [countries, setCountries] = useState(initialCountries);

  useEffect(() => {
    fetchCountries(appendCountries);
  }, []);

  return (
    <CountryContext.Provider value={countries}>
      {props.children}
    </CountryContext.Provider>
  );
};
