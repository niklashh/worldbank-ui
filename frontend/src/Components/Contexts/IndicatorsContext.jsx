import React, {useState} from 'react';
import {merge} from 'lodash';

import {fetchIndicator} from '../../utils';

export const IndicatorsContext = React.createContext();

export const IndicatorsProvider = props => {
  const initialOptions = {};
  const [indicatorData, setIndicatorData] = useState(initialOptions);
  // console.debug('IndicatorsProvider');

  /*
   * IndicatorData will have the following structure
   * {
   *   FIN: {
   *     population: {
   *       1960: 4400000,
   *       etc.
   *     },
   *     emissions: {
   *       1960: 15100,
   *       etc.
   *     }
   *   },
   *   GBR: etc.
   * }
   * If the country has no data (is null)
   * the indicator is just an empty object {}
   */
  const appendData = (countryId, indicator, data) => {
    // (isEqual(indicatorData, merged) && console.log('equal')) ||
    setIndicatorData(prevData =>
      // merge mutates
      merge({}, prevData, {
        [countryId]: {
          [indicator]: data
            ? data.reduce((map, obj) => {
                map[obj.date] = obj.value;
                return map;
              }, {})
            : {},
        },
      }),
    );
  };

  const fetchData = fetchIndicator(appendData);

  return (
    <IndicatorsContext.Provider value={{indicatorData, fetchData}}>
      {props.children}
    </IndicatorsContext.Provider>
  );
};
