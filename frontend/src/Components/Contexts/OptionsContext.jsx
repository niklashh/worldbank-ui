import React, {useReducer} from 'react';
import {blue} from '@material-ui/core/colors';
import {merge} from 'lodash';

export const OptionsContext = React.createContext();

// TODO externalize reducer from this function,
// create functions that use dispatch
const reducer = (state, action) => {
  switch (action.type) {
    case 'setDark':
      return merge({}, state, {palette: {type: action.value}});
    case 'selectCountries':
      return {...state, selectedCountries: action.value};
    case 'selectIndicators':
      return {...state, showIndicators: action.value};
    case 'setNormalizeToPopulation':
      return {...state, normalizeToPopulation: action.value};
    default:
      throw new Error();
  }
};

export const OptionsProvider = props => {
  // console.debug('OptionsProvider');
  const initialOptions = {
    selectedCountries: [
      // {id: 'FIN', name: 'Finland', iso2Code: 'FI'},
      // {id: 'SWE', name: 'Sweden', iso2Code: 'SW'},
    ],
    palette: {
      type: 'dark',
      primary: blue,
    },
    showIndicators: {population: true, emissions: false},
    normalizeToPopulation: false,
  };
  const [options, dispatch] = useReducer(reducer, initialOptions);

  const setDark = value => dispatch({type: 'setDark', value});
  const selectCountries = country =>
    dispatch({
      type: 'selectCountries',
      value: country,
    });
  const selectIndicators = value => dispatch({type: 'selectIndicators', value});
  const setNormalizeToPopulation = value =>
    dispatch({type: 'setNormalizeToPopulation', value});

  return (
    <OptionsContext.Provider
      value={{
        ...options,
        options: {
          setDark,
          selectCountries,
          selectIndicators,
          setNormalizeToPopulation,
        },
      }}>
      {props.children}
    </OptionsContext.Provider>
  );
};
