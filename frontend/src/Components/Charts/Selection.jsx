import React, {useContext} from 'react';
import {
  Checkbox,
  FormControlLabel,
  FormLabel,
  Paper,
  Grid,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';

import {OptionsContext} from '../Contexts';

const useStyles = makeStyles(theme => ({
  paper: {
    alignItems: 'center',
    padding: theme.spacing.unit,
    paddingBottom: 0,
  },
}));

export const SelectIndicators = () => {
  const classes = useStyles();
  const {
    normalizeToPopulation,
    showIndicators,
    options: {selectIndicators, setNormalizeToPopulation},
  } = useContext(OptionsContext);

  return (
    <Paper elevation={1} className={classes.paper}>
      <FormLabel component="legend">Change what data is shown</FormLabel>
      <Grid container>
        {['population', 'emissions'].map(ind => (
          <Grid item key={ind} xs={12} sm={6}>
            <FormControlLabel
              label={`Show ${ind}`}
              control={
                <Checkbox
                  color={ind === 'population' ? 'primary' : 'secondary'}
                  onChange={event =>
                    selectIndicators({
                      ...showIndicators,
                      [ind]: event.target.checked,
                    })
                  }
                  checked={showIndicators[ind]}
                />
              }
            />
          </Grid>
        ))}
        <Grid item xs={12} sm={6}>
          <FormControlLabel
            label="Normalize data to population"
            control={
              <Checkbox
                color="primary"
                onChange={event =>
                  setNormalizeToPopulation(event.target.checked)
                }
                checked={normalizeToPopulation}
              />
            }
          />
        </Grid>
      </Grid>
    </Paper>
  );
};
