import React, {useContext} from 'react';
import {Typography} from '@material-ui/core';
import {makeStyles, useTheme} from '@material-ui/styles';
import {toHumanString} from 'human-readable-numbers';

import {OptionsContext, IndicatorsContext} from '../Contexts';
import {Line, defaults} from 'react-chartjs-2';
import {formatEmissions, formatPopulation, shiftColor} from '../../utils';

const useStyles = makeStyles(theme => ({
  chartContainer: {
    '& canvas': {marginTop: 2 * theme.spacing.unit},
  },
}));

const Charts = React.memo(() => {
  // console.debug('Charts');
  const classes = useStyles();
  const theme = useTheme();
  const {normalizeToPopulation, showIndicators, selectedCountries} = useContext(
    OptionsContext,
  );
  const {indicatorData} = useContext(IndicatorsContext);
  // const prevSelectedCountry = usePrevious(selectedCountry);

  if (
    // XXX what if empty?
    selectedCountries.every(country =>
      areIndicatorsLoaded(country, indicatorData),
    )
  ) {
    const labels = Array.from(
      selectedCountries
        .map(
          country =>
            new Set(Object.keys(indicatorData[country.id]['population'])),
        )
        // XXX Order of a set, does it matter?
        .reduce((z, set) => new Set([...set, ...z]), []),
    );
    // Map over country first
    const datasets = selectedCountries.flatMap((country, idx) =>
      Object.entries(showIndicators)
        // Filter out hidden indicators
        .filter(([ind, show]) => show === true)
        .map(([ind, _]) => ({
          data: labels.map(year => {
            const point = indicatorData[country.id][ind][year];
            if (point === null) return null;
            if (normalizeToPopulation) {
              const pop = indicatorData[country.id]['population'][year];
              if (pop === null) return null;
              return point / pop;
            }
            return point;
          }),
          label: `${country.name} - ${ind}`,
          id: country.id,
          ind: ind,
          fill: false,
          lineTension: 0.1,
          borderColor:
            ind === 'population'
              ? shiftColor(theme.palette.primary.light, 15 * idx)
              : shiftColor(theme.palette.secondary.light, 15 * idx),
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          yAxisID: ind === 'population' ? 'populationAxis' : 'emissionsAxis',
        })),
    );

    defaults.global.defaultColor = theme.palette.text.primary;
    defaults.global.defaultFontColor = theme.palette.text.secondary;
    const options = {
      maintainAspectRatio: true,
      // legend: {display: false},
      scales: {
        yAxes: [
          {
            id: 'populationAxis',
            display: showIndicators.population,
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: 'Population',
            },
            gridLines: {
              display: showIndicators.population && !showIndicators.emissions,
            },
            ticks: {
              callback: (value, index, values) =>
                normalizeToPopulation
                  ? value === 1
                    ? 1
                    : ''
                  : toHumanString(value),
            },
          },
          {
            id: 'emissionsAxis',
            display: showIndicators.emissions,
            position: 'right',
            scaleLabel: {
              display: true,
              labelString: 'CO2 Emissions',
            },
            gridLines: {
              display: showIndicators.emissions,
            },
            ticks: {
              callback: (value, index, values) =>
                toHumanString(1000 * value) + 't',
            },
          },
        ],
      },
      tooltips: {
        callbacks: {
          title: ([tooltipItem]) => {
            const date = tooltipItem.xLabel;
            const country = datasets[tooltipItem.datasetIndex].label;
            return `${country} @ ${date}`;
          },
          label: tooltipItem => {
            const date = tooltipItem.xLabel;
            const {id, ind} = datasets[tooltipItem.datasetIndex];
            const population = indicatorData[id].population[date];
            const emissions = indicatorData[id].emissions[date];
            if (ind === 'population') {
              return `Total Population: ${formatPopulation(population)}`;
            } else {
              return `Total CO2 Emissions: ${formatEmissions(emissions)}`;
            }
          },
          footer: ([tooltipItem]) => {
            const date = tooltipItem.xLabel;
            const {id, ind} = datasets[tooltipItem.datasetIndex];
            const population = indicatorData[id].population[date];
            const emissions = indicatorData[id].emissions[date];
            if (ind === 'population') {
              return '';
            } else {
              return `CO2 Emissions: ${formatEmissions(
                emissions / population,
              )} per Capita`;
            }
          },
        },
        footerFontStyle: 'normal',
      },
    };
    return (
      <div className={classes.chartContainer}>
        <Line
          data={{
            labels: labels,
            datasets: datasets,
          }}
          options={options}
        />
      </div>
    );
  }
  return <NoCountrySelected />;
});

const NoCountrySelected = props => {
  return <Typography>Select a country</Typography>;
};

const areIndicatorsLoaded = (country, indicatorData) =>
  indicatorData[country.id] &&
  ['population', 'emissions'].every(ind =>
    indicatorData[country.id].hasOwnProperty(ind),
  );

export default Charts;
