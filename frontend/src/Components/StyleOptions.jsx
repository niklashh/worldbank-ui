import React, {useContext, memo} from 'react';
import {Switch, Tooltip} from '@material-ui/core';

import {OptionsContext} from './Contexts';

export const ToggleDark = memo(() => {
  const {
    palette,
    options: {setDark},
  } = useContext(OptionsContext);

  return (
    <Tooltip title="Toggle dark theme">
      <Switch
        checked={palette.type === 'dark'}
        onChange={event => setDark(event.target.checked ? 'dark' : 'light')}
      />
    </Tooltip>
  );
});
