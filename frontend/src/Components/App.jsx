import React, {useContext, useState, useEffect, useRef} from 'react';
import './App.css';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {ThemeProvider, makeStyles, useTheme} from '@material-ui/styles';
import {Grid, Typography, CircularProgress} from '@material-ui/core';
import {unstable_useMediaQuery as useMediaQuery} from '@material-ui/core/useMediaQuery';
import {isEqual} from 'lodash';

import {ToggleDark} from './StyleOptions';
import {Header, Page} from './Layouts';
import {
  CountryProvider,
  OptionsProvider,
  OptionsContext,
  IndicatorsProvider,
  IndicatorsContext,
} from './Contexts';
import {Charts, SelectIndicators} from './Charts';
import {AutocompleteSearch} from './Search';

const useStyles = makeStyles(theme => ({
  gridContainer: {
    padding: theme.spacing.unit,
  },
  gridItem: {
    width: '100%',
  },
  title: {
    whiteSpace: 'nowrap',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      textAlign: 'center',
    },
  },
  loader: {
    position: 'absolute',
    left: '50%',
    marginLeft: -20,
    marginTop: theme.spacing.unit,
  },
}));

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const App = props => {
  const classes = useStyles();
  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.only('xs'));

  const [isLoading, setLoading] = useState(false);
  const {fetchData} = useContext(IndicatorsContext);
  const {selectedCountries} = useContext(OptionsContext);
  const prevSelectedCountries = usePrevious(selectedCountries);
  useEffect(() => {
    if (!isLoading && !isEqual(prevSelectedCountries, selectedCountries)) {
      setLoading(true);
      Promise.all(
        selectedCountries.map(country => {
          return Promise.all([
            fetchData(country.id, 'population'),
            fetchData(country.id, 'emissions'),
          ]);
        }),
      )
        .catch(error => console.log('error loading indicator data', error))
        // .then(() => console.log('indicator data is loaded'))
        .then(() => setLoading(false));
    }
  }, [selectedCountries, isLoading]);

  const SearchComponent = <AutocompleteSearch inHeader={!isXs} />;

  return (
    <Page>
      <Header>
        {isXs ? <div style={{flexGrow: 1}} /> : ''}
        <Typography className={classes.title} variant="h6" color="inherit">
          WorldBank-ui
        </Typography>
        <div style={{flexGrow: 1}} />
        {!isXs ? SearchComponent : ''}
        <ToggleDark />
      </Header>
      <Grid
        container
        spacing={8}
        classes={{container: classes.gridContainer}}
        direction="column">
        {isXs ? (
          <Grid item className={classes.gridItem}>
            {SearchComponent}
          </Grid>
        ) : (
          ''
        )}
        <Grid item className={classes.gridItem}>
          <SelectIndicators />
          {isLoading ? <CircularProgress className={classes.loader} /> : ''}
          <Charts />
        </Grid>
      </Grid>
    </Page>
  );
};

const Providers = props => {
  const options = useContext(OptionsContext);
  const theme = createMuiTheme({
    palette: options.palette,
    typography: {
      useNextVariants: true,
    },
  });

  return (
    <CountryProvider>
      <IndicatorsProvider>
        <MuiThemeProvider theme={theme}>
          <ThemeProvider theme={theme}>
            <App />
          </ThemeProvider>
        </MuiThemeProvider>
      </IndicatorsProvider>
    </CountryProvider>
  );
};

export default props => (
  <OptionsProvider>
    <Providers />
  </OptionsProvider>
);
