import axios from 'axios';
import {toHumanString} from 'human-readable-numbers';
import {
  decomposeColor,
  recomposeColor,
} from '@material-ui/core/styles/colorManipulator';

// const HOST = process.env.REACT_APP_API_HOST;
const HOST = window.location.hostname;
const PREFIX = process.env.REACT_APP_API_PREFIX;

export const fetchCountries = appender =>
  axios(`http://${HOST}/${PREFIX}/countries/filterUseless?page=1`)
    .then(response => response.data)
    .then(data => {
      appender(data[1]);
      const {pages} = data[0];
      return Promise.all(
        Array(pages - 1)
          .fill()
          .map((_, index) => {
            const page = index + 2;
            return axios(
              `http://${HOST}/${PREFIX}/countries/filterUseless?page=${page}`,
            )
              .then(response => response.data[1])
              .then(appender);
          }),
      );
    })
    .catch(error => {
      console.log('Error getting data', error);
    });

export const fetchIndicator = appender => (countryId, indicator) =>
  axios(
    `http://${HOST}/${PREFIX}/${indicator}/filterUseless/${countryId}?page=1`,
  )
    .then(response => response.data)
    .then(data => {
      const {pages} = data[0];
      appender(countryId, indicator, data[1]);
      if (pages === 0) {
        return;
      }
      return Promise.all(
        Array(pages - 1)
          .fill()
          .map((_, index) => {
            const page = index + 2;
            return axios(
              `http://${HOST}/${PREFIX}/${indicator}/filterUseless/${countryId}?page=${page}`,
            )
              .then(response => response.data[1])
              .then(data => appender(countryId, indicator, data));
          }),
      );
    })
    .catch(error => {
      console.log('Error getting data', error);
    });

export const formatPopulation = number =>
  toHumanString(number)
    .replace('k', ' Thousand')
    .replace('M', ' Million')
    .replace('G', ' Billion');

export const formatEmissions = number => {
  if (number === null) return 'Not available';

  const exp = (1000 * number).toFixed().toString().length;
  if (10 <= exp && exp <= 12) {
    return `${(number / 1000000).toFixed(1)} Gigatonnes`;
  }
  if (7 <= exp && exp <= 9) {
    return `${(number / 1000).toFixed(1)} Megatonnes`;
  } else if (4 <= exp && exp <= 6) {
    return `${number.toFixed(1)} Kilotonnes`;
  } else if (2 <= exp && exp <= 3) {
    return `${(number * 1000).toFixed(1)} Tonnes`;
  } else if (exp === 1) {
    return `${(number * 1000000).toFixed(1)} Kilograms`;
  } else {
    return `${number.toFixed(1)} Kilotonnes`;
  }
};

function rgbToHsl(r, g, b) {
  r /= 255;
  g /= 255;
  b /= 255;
  var max = Math.max(r, g, b),
    min = Math.min(r, g, b);
  var h,
    s,
    l = (max + min) / 2;
  if (max === min) {
    h = s = 0; // achromatic
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
      default:
        throw new Error();
    }
    h /= 6;
  }
  return [360 * h, 100 * s, 100 * l];
}

function hslToRgb(h, s, l) {
  h /= 360;
  s /= 100;
  l /= 100;
  var r, g, b;
  if (s === 0) {
    r = g = b = l; // achromatic
  } else {
    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    }
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }
  return [r * 255, g * 255, b * 255];
}

export const shiftColor = (color, shiftDegrees) =>
  recomposeColor({
    type: 'rgb',
    values: hslToRgb(
      ...rgbToHsl(...decomposeColor(color).values).map((v, i) =>
        i === 0 ? v + shiftDegrees : v,
      ),
    ),
  });
