const http = require('http');
const app = require('./app');

const PORT = process.env.API_PORT || 8080;
const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}!`);
});

// vim: et ts=2 sw=2 :
