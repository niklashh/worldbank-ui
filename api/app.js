const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const swaggerJSDoc = require('swagger-jsdoc');
const serializeError = require('serialize-error');

const {
  populationRouter,
  emissionsRouter,
  countriesRouter,
  clearCacheRouter,
} = require('./routes');
const {redisClient} = require('./utils');

const PREFIX = process.env.API_PREFIX;
const HOST = process.env.API_HOST;

const app = express();
const swaggerDefinition = {
  info: {
    title: 'WorldBank-UI API',
    version: '0.4.1',
    description: 'API documentation for the express backend',
  },
  host: HOST,
  basePath: `/${PREFIX}`,
};
const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./routes/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Serve swagger.json
app.get(`/${PREFIX}/swagger.json`, function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use(`/${PREFIX}/population`, populationRouter);
app.use(`/${PREFIX}/emissions`, emissionsRouter);
app.use(`/${PREFIX}/countries`, countriesRouter);
app.use(`/${PREFIX}/clearCache`, clearCacheRouter);

// Not found handler
app.use((req, res) => {
  res.status(404).json({
    message: `Route not found: ${req.method} ${req.url}`,
    api_host: HOST,
    api_prefix: PREFIX,
  });
});

// Generic error handler
app.use((err, req, res, _) => {
  const status = err.status || 500;
  if (status === 500) {
    console.log(err);
    res.status(500).json({
      message:
        'Something went wrong... Please submit an issue at ' +
        '`https://gitlab.com/niklashh/worldbank-ui/issues`',
      error: serializeError(err),
    });
  } else {
    res.status(status).json({
      // error: serializeError(err),
      message: err.message,
    });
  }
});

module.exports = app;

// vim: et ts=2 sw=2 :
