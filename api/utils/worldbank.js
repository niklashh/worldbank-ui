const axios = require('axios');
const hash = require('object-hash');
const querystring = require('querystring');
const {Map} = require('immutable');

const redisClient = require('./redis');

// How long cache values live (seconds)
const REDIS_EXPIRY = process.env.REDIS_EXPIRY || 60;

const handleWorldbankApiError = error => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx

    error.status = error.response.status;
    if (error.status === 500) {
      // If worldbank api decides to throw a curveball at you
      error.status = 400;
    }
    error.message = `Worldbank api returned an error.
Known causes: - invalid country/area id - the request contains non-ascii characters`;
  } else if (error.request) {
    // The request was made but no response was received
    console.log(`No response from Worldbank!`);
    error.message = 'Endpoint unreachable';
  } else {
    // Something happened in setting up the request that triggered an Error
    if (error.message === 'Request path contains unescaped characters') {
      error.status = 400;
    }
  }
  console.log(`Worldbank error status ${error.status}`);
  return Promise.reject(error);
};

const getWorldbankIndicator = (indicator, code, query) =>
  axios
    .get(
      'https://api.worldbank.org/v2/country/' +
        `${code}/indicator/${indicator}?${query}`,
    )
    .catch(handleWorldbankApiError);

const getWorldbankCountries = query =>
  axios
    .get(`https://api.worldbank.org/v2/country?${query}`)
    .catch(handleWorldbankApiError);

const getHash = (path, query, name, code) => hash({path, query, name, code});

const setCache = (key, stringified) =>
  redisClient.setex(key, REDIS_EXPIRY, stringified);

const getCache = key => redisClient.get(key);

const getCountries = options =>
  new Promise((resolve, reject) => {
    const query = querystring.stringify(
      Map(options)
        .set('format', 'json')
        .toJS(),
    );
    const key = getHash('countries', query);
    return getCache(key).then(result => {
      // If the key is in the cache
      if (result !== null) {
        resolve({stringified: result});
      } else {
        getWorldbankCountries(query)
          .then(data => {
            // Cache the response if it was successful
            const stringified = JSON.stringify(data.data);
            return setCache(key, stringified).then(() =>
              resolve({stringified, data: data.data}),
            );
          })
          .catch(reject);
      }
    });
  });

const getCountryIndicator = (path, indicator, code, options) =>
  new Promise((resolve, reject) => {
    const query = querystring.stringify(
      Map(options)
        .set('format', 'json')
        .toJS(),
    );
    const key = getHash(path, query, indicator, code);
    return getCache(key).then(result => {
      // If the key is in the cache
      if (result !== null) {
        resolve({stringified: result});
      } else {
        getWorldbankIndicator(indicator, code, query)
          .then(data => {
            const stringified = JSON.stringify(data.data);
            return setCache(key, stringified).then(() =>
              resolve({stringified, data: data.data}),
            );
          })
          .catch(reject);
      }
    });
  });

const getErrorCode = data =>
  [0, 'message', 0, 'id'].reduce(
    (rest, cur) => (rest && rest[cur]) || null,
    data,
  );

const mapErrorStatus = errorCode =>
  ({
    105: 503,
    110: 404,
    111: 404,
    112: 404,
    115: 404,
    120: 404,
    140: 400,
    150: 400,
    160: 400,
    199: 500,
  }[errorCode]);

const minifyIndicatorData = data => [
  data[0],
  data[1] &&
    data[1].map(piece => ({
      value: piece.value,
      date: piece.date,
    })),
];

const minifyCountryData = data => [
  data[0],
  data[1] &&
    data[1].map(piece => ({
      id: piece.id,
      iso2Code: piece.iso2Code,
      name: piece.name,
    })),
];

module.exports = {
  getCountryIndicator,
  getCountries,
  minifyIndicatorData,
  minifyCountryData,
  getErrorCode,
  mapErrorStatus,
};

// vim: et ts=2 sw=2 :
