const redisClient = require('./redis');
const worldbank = require('./worldbank');

module.exports = {redisClient, worldbank};

// vim: et ts=2 sw=2 :
