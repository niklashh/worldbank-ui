const redis = require('async-redis');

const client = redis.createClient({
  host: 'redis',
  port: 6379,
});
console.log(`Redis connected @ ${client.address}`);

module.exports = client;

// vim: et ts=2 sw=2 :
