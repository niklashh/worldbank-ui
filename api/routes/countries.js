const express = require('express');

const {worldbank} = require('../utils');

const router = express.Router();
module.exports = router;

/**
 * @swagger
 * definitions:
 *   CountryMetadata:
 *     properties:
 *       page:
 *         type: integer
 *         example: 3
 *       pages:
 *         type: integer
 *         example: 7
 *       per_page:
 *         type: string
 *         example: "49"
 *       total:
 *         type: integer
 *         example: 304
 *   CountryData:
 *     properties:
 *       id:
 *         type: string
 *         example: "FIN"
 *         description: The country's ISO 3 code
 *       iso2code:
 *         type: string
 *         example: "FI"
 *       name:
 *         type: string
 *         example: "Finland"
 *       region:
 *         type: object
 *         properties:
 *           id:
 *             type: string
 *             example: "ECS"
 *           iso2code:
 *             type: string
 *             example: "Z7"
 *           value:
 *             type: string
 *             example: "Europe & Central Asia"
 *       adminregion:
 *         type: object
 *         properties:
 *           id:
 *             type: string
 *             example: ""
 *           iso2code:
 *             type: string
 *             example: ""
 *           value:
 *             type: string
 *             example: ""
 *       incomeLevel:
 *         type: object
 *         properties:
 *           id:
 *             type: string
 *             example: "HIC"
 *           iso2code:
 *             type: string
 *             example: "XD"
 *           value:
 *             type: string
 *             example: "High income"
 *       lendingType:
 *         type: object
 *         properties:
 *           id:
 *             type: string
 *             example: "LNX"
 *           iso2code:
 *             type: string
 *             example: "XX"
 *           value:
 *             type: string
 *             example: "Not classified"
 *       capitalCity:
 *         type: string
 *         example: "Helsinki"
 *       longitude:
 *         type: string
 *         example: "24.9525"
 *       latitude:
 *         type: string
 *         example: "60.1608"
 */

/**
 * @swagger
 * /countries/{options}:
 *   get:
 *     tags:
 *       - Countries
 *     summary:
 *     description: >
 *       Use the route `/countries/filterUseless` to filter out useless data
 *
 *       An example **`CountryData`** document with the filter applied:
 *
 *       ```
 *
 *       {
 *         "id": "FIN",
 *         "iso2Code": "FI",
 *         "name": "Finland"
 *       }
 *
 *       ```
 *     parameters:
 *       - in: path
 *         name: options
 *         type: string
 *         description: An option to filter the data
 *       - in: query
 *         name: page
 *         type: integer
 *         description: The number of the page to source
 *       - in: query
 *         name: per_page
 *         type: integer
 *         description: The number of data documents per page
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A list with metadata and countries
 *         schema:
 *           type: array
 *           items:
 *             oneOf:
 *               - $ref: '#/definitions/CountryMetadata'
 *               - type: array
 *                 items:
 *                   $ref: '#/definitions/CountryData'
 *       404:
 *         description: Route not found or a value is invalid
 *       503, 500:
 *         $ref: '#/responses/WorldbankError'
 */
router.get('/:options(filterUseless)?/', (req, res, next) => {
  worldbank
    .getCountries(req.query)
    .then(result => {
      const {stringified} = result;
      const data = result.data || JSON.parse(stringified);

      if (req.params.options === 'filterUseless') {
        const minified = worldbank.minifyCountryData(data);
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .json(minified);
      } else {
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .send(stringified);
      }
    })
    .catch(next);
});

// vim: et ts=2 sw=2 :
