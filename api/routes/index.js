const populationRouter = require('./population');
const emissionsRouter = require('./emissions');
const countriesRouter = require('./countries');
const clearCacheRouter = require('./clearCache');

module.exports = {
  populationRouter,
  emissionsRouter,
  countriesRouter,
  clearCacheRouter,
};

// vim: et ts=2 sw=2 :
