const express = require('express');

const {worldbank} = require('../utils');

const router = express.Router();
module.exports = router;

/**
 * @swagger
 * /emissions/{options}/{code}:
 *   get:
 *     tags:
 *       - Indicators
 *     summary: Get the country's/area's CO2 emissions data from WorldBank
 *     description: >
 *       Use the route `/emissions/filterUseless/{code}` to
 *       filter out useless data
 *
 *       An example **`IndicatorData`** document with the filter applied:
 *
 *       ```bash
 *
 *       {
 *         "value": null,
 *         "date": "2018"
 *       }
 *
 *       ```
 *     parameters:
 *       - in: path
 *         name: options
 *         type: string
 *         description: An option to filter the data
 *       - in: path
 *         name: code
 *         type: string
 *         required: true
 *         description: ISO 2 or ISO 3 code for the country/area
 *       - in: query
 *         name: page
 *         type: integer
 *         description: The number of the page to source
 *       - in: query
 *         name: per_page
 *         type: integer
 *         description: The number of data documents per page
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A list with metadata and the data
 *         schema:
 *           type: array
 *           items:
 *             oneOf:
 *               - $ref: '#/definitions/IndicatorMetadata'
 *               - type: array
 *                 items:
 *                   $ref: '#/definitions/IndicatorData'
 *       400:
 *         description: Parsing the request failed
 *       404:
 *         description: Route not found or a value is invalid
 *       503, 500:
 *         $ref: '#/responses/WorldbankError'
 */
router.get('/:options(filterUseless)?/:code/', (req, res, next) => {
  worldbank
    .getCountryIndicator(
      'emissions',
      'EN.ATM.CO2E.KT',
      req.params.code,
      req.query,
    )
    .then(result => {
      const {stringified} = result;
      const data = result.data || JSON.parse(stringified);

      const errorCode = worldbank.getErrorCode(data);
      if (errorCode) {
        res
          .status(worldbank.mapErrorStatus(errorCode))
          .header('Content-Type', 'application/json')
          .send(stringified);
      } else if (req.params.options === 'filterUseless') {
        const minified = worldbank.minifyIndicatorData(data);
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .json(minified);
      } else {
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .send(stringified);
      }
    })
    .catch(next);
});

// vim: et ts=2 sw=2 :
