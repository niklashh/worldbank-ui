const express = require('express');

const {worldbank} = require('../utils');

const router = express.Router();
module.exports = router;

/**
 * @swagger
 * definitions:
 *   IndicatorMetadata:
 *     properties:
 *       page:
 *         type: integer
 *         example: 1
 *       pages:
 *         type: integer
 *         example: 5
 *       per_page:
 *         type: integer
 *         example: 5
 *       total:
 *         type: integer
 *         example: 23
 *       sourceid:
 *         type: string
 *         nullable: true
 *         example: "2"
 *       lastupdated:
 *         type: string
 *         nullable: true
 *         format: date
 *         example: "2019-1-30"
 *   IndicatorData:
 *     properties:
 *       indicator:
 *         type: object
 *         properties:
 *           id:
 *             type: string
 *             example: "SP.POP.TOTL"
 *           value:
 *             type: string
 *             example: "Population, total"
 *       country:
 *         type: object
 *         properties:
 *           id:
 *             description: The country's ISO 2 code
 *             type: string
 *             example: "FI"
 *           value:
 *             type: string
 *             example: "Finland"
 *       countryiso3code:
 *         type: string
 *         example: "FIN"
 *       date:
 *         type: string
 *         example: "2016"
 *       value:
 *         type: integer
 *         nullable: true
 *         example: 5495303
 * responses:
 *   WorldbankError:
 *     description: >
 *       A communication failure with WorldBank
 *
 *       see [WorldBank API Error Codes](https://datahelpdesk.worldbank.org/knowledgebase/articles/898620-api-error-codes)
 *       for more detail
 */

/**
 * @swagger
 * /population/{options}/{code}:
 *   get:
 *     tags:
 *       - Indicators
 *     summary: Get the country's/area's population data from WorldBank
 *     description: >
 *       Use the route `/population/filterUseless/{code}` to
 *       filter out useless data
 *
 *       An example **`IndicatorData`** document with the filter applied:
 *
 *       ```bash
 *
 *       {
 *         "value": 47300.633,
 *         "date": "2014"
 *       }
 *
 *       ```
 *     parameters:
 *       - in: path
 *         name: options
 *         type: string
 *         description: An option to filter the data
 *       - in: path
 *         name: code
 *         type: string
 *         required: true
 *         description: ISO 2 or ISO 3 code for the country/area
 *       - in: query
 *         name: page
 *         type: integer
 *         description: The number of the page to source
 *       - in: query
 *         name: per_page
 *         type: integer
 *         description: The number of data documents per page
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A list with metadata and the data
 *         schema:
 *           type: array
 *           items:
 *             oneOf:
 *               - $ref: '#/definitions/IndicatorMetadata'
 *               - type: array
 *                 items:
 *                   $ref: '#/definitions/IndicatorData'
 *       400:
 *         description: Parsing the request failed
 *       404:
 *         description: Route not found or a value is invalid
 *       503, 500:
 *         $ref: '#/responses/WorldbankError'
 */
router.get('/:options(filterUseless)?/:code/', (req, res, next) => {
  worldbank
    .getCountryIndicator(
      'population',
      'SP.POP.TOTL',
      req.params.code,
      req.query,
    )
    .then(result => {
      const {stringified} = result;
      const data = result.data || JSON.parse(stringified);

      const errorCode = worldbank.getErrorCode(data);
      if (errorCode) {
        res
          .status(worldbank.mapErrorStatus(errorCode))
          .header('Content-Type', 'application/json')
          .send(stringified);
      } else if (req.params.options === 'filterUseless') {
        const minified = worldbank.minifyIndicatorData(data);
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .json(minified);
      } else {
        res
          .status(200)
          .header('Content-Type', 'application/json')
          .send(stringified);
      }
    })
    .catch(next);
});

// vim: et ts=2 sw=2 :
