const express = require('express');

const {worldbank, redisClient} = require('../utils');

const router = express.Router();
module.exports = router;

router.get('/', (req, res, next) => {
  redisClient
    .flushall()
    .then(result => res.status(200).json({message: result}))
    .catch(next);
});

// vim: et ts=2 sw=2 :
